package com.itfruit.jmcutils.aggregator;

import com.itfruit.jmcutils.config.AgentConfig;
import com.itfruit.jmcutils.executors.CatchingRunnable;
import com.itfruit.jmcutils.executors.ScheduledExecutorFactory;
import com.itfruit.jmcutils.results.FlameGraphFlatResultsWriter;
import com.itfruit.jmcutils.tree.TreeWithValue;

import java.math.BigInteger;
import java.nio.file.Path;
import java.time.Duration;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.function.Supplier;
import java.util.stream.Collectors;

public class ResultsAggregateRotate {
    private final ScheduledExecutorService executor = ScheduledExecutorFactory.setupScheduledPool("Results Aggregate-Rotate");
    private final FlameGraphFlatResultsWriter resultsWriter;
    private final Map<Duration, TreeWithValue<String, BigInteger>> durationToTree = new ConcurrentHashMap<>();

    public ResultsAggregateRotate(AgentConfig agentConfig, Supplier<List<TreeWithValue<String, BigInteger>>> treeSupplier) {
        resultsWriter = new FlameGraphFlatResultsWriter(agentConfig);

        Runtime.getRuntime().addShutdownHook(new Thread(this::shutdown));

        var intervals = agentConfig.getIntervals();
        var durationList = intervals.getAggregateRotateResults().stream().sorted().collect(Collectors.toList());

        executor.scheduleAtFixedRate(new CatchingRunnable(() -> aggregateTree(treeSupplier.get())),
                500, 500, TimeUnit.MILLISECONDS);

        for (Duration duration : durationList) {
            durationToTree.put(duration, new TreeWithValue<>());
            executor.scheduleAtFixedRate(new CatchingRunnable(() -> this.aggregate(duration)),
                    duration.toSeconds(), duration.toSeconds(), TimeUnit.SECONDS);
        }
    }

    private void aggregate(Duration duration) {
        var tree = durationToTree.get(duration);

        durationToTree.put(duration, new TreeWithValue<>());

        resultsWriter.writeToFile(tree, Path.of("results-" + duration.toString()));
    }

    private void aggregateTree(List<TreeWithValue<String, BigInteger>> treeList) {
        treeList.forEach(tree ->
                durationToTree.values().forEach(existingTree ->
                        existingTree.merge(tree, BigInteger::add)));
    }

    public void shutdown() {
        executor.shutdownNow();
    }

}
