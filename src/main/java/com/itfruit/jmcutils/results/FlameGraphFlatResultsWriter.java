package com.itfruit.jmcutils.results;

import com.itfruit.jmcutils.config.AgentConfig;
import com.itfruit.jmcutils.tree.TreeWithValue;
import net.lingala.zip4j.ZipFile;
import net.lingala.zip4j.model.ZipParameters;

import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.time.LocalDateTime;
import java.util.concurrent.atomic.AtomicBoolean;

public class FlameGraphFlatResultsWriter {
    private final AgentConfig agentConfig;

    public FlameGraphFlatResultsWriter(AgentConfig agentConfig) {
        this.agentConfig = agentConfig;
    }

    public void writeToFile(TreeWithValue<String, BigInteger> tree, Path location) {
        var path = Path.of(agentConfig.getResultPath()).resolve(location);

        try (BufferedWriter writer = Files.newBufferedWriter(path, StandardCharsets.US_ASCII,
                StandardOpenOption.APPEND, StandardOpenOption.CREATE)) {

            // Flame graph style
            tree.traverse((iterator, value) -> {
                AtomicBoolean isFirst = new AtomicBoolean(true);
                while (iterator.hasNext()) {
                    var key = iterator.next();
                    if (!isFirst.get()) {
                        writer.append(";");
                    } else {
                        isFirst.set(false);
                    }
                    writer.append(key);
                }

                writer.append(" ");
                writer.append(value.toString());
                writer.append("\n");
            });
        } catch (Exception e) {
            System.err.println("Failed to write sample to file");
            e.printStackTrace();
        }

        // rotate to zip if above size
        var file = path.toFile();
        if (file.length() > (agentConfig.getLimits().getRotateToZipAfterMegabytes() * 1_000_000)) {
            writeToZip(location, file);

            file.delete();
        }
    }

    private void writeToZip(Path path, File fileToAdd) {
        ZipParameters zipParameters = new ZipParameters();
        zipParameters.setFileNameInZip(path.toString() + "-" + LocalDateTime.now().toString());

        try {
            var zipFile = new ZipFile(Path.of(agentConfig.getResultPath()).resolve(path.toString() + ".zip").toFile());
            zipFile.addFile(fileToAdd, zipParameters);
        } catch (IOException e) {
            System.err.println("Failed to rotate to zip");
            e.printStackTrace();
        }
    }
}
