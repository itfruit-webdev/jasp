package com.itfruit.jmcutils.thread;

import com.itfruit.jmcutils.config.AgentConfig;
import com.itfruit.jmcutils.executors.CatchingRunnable;
import com.itfruit.jmcutils.executors.NamedThreadFactory;
import com.itfruit.jmcutils.executors.ScheduledExecutorFactory;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

public class ThreadDumper {

    private final ScheduledExecutorService threadDumperExecutor = ScheduledExecutorFactory.setupScheduledPool("Thread Dumper");
    private final AgentConfig agentConfig;
    private final BlockingQueue<ThreadAndStackTrace> dumpQueue;


    public ThreadDumper(AgentConfig agentConfig) {
        this.agentConfig = agentConfig;

        dumpQueue = new ArrayBlockingQueue<>(calcMaxQueue());

        Runtime.getRuntime().addShutdownHook(new Thread(this::shutdown));

        threadDumperExecutor.scheduleAtFixedRate(new CatchingRunnable(this::threadDumper), 1000,
                agentConfig.getIntervals().getThreadDump().toMillis(), TimeUnit.MILLISECONDS);
    }

    public void shutdown() {
        threadDumperExecutor.shutdownNow();
    }

    public List<ThreadAndStackTrace> retrieveDumps() {
        var list = new LinkedList<ThreadAndStackTrace>();
        dumpQueue.drainTo(list);
        return list;
    }

    private void threadDumper() throws InterruptedException {
        var stackTraces = Thread.getAllStackTraces();

        var threadToCount = stackTraces.keySet().stream()
                .collect(Collectors.groupingBy(Thread::getName, Collectors.counting()));

        Map<String, List<StackTraceElement>> threadToStackTraceElements = stackTraces
                .entrySet()
                .stream()
                // Exclude itself
                .filter(e -> !NamedThreadFactory.isAgentThread(e.getKey().getName()))
                .filter(e -> excludeIncludeThreads(e.getKey().getName()))
                .collect(Collectors.toMap(e -> {
                    var threadName = e.getKey().getName();
                    var sameThreadCount = threadToCount.getOrDefault(threadName, 1L);
                    if (sameThreadCount > 1) {
                        return threadName + "(" + e.getKey().getId() + ")";
                    }
                    return threadName;
                }, e -> List.of(e.getValue())));

        var threadAndStackTrace = ThreadAndStackTrace.builder()
                .timeInNanos(System.nanoTime())
                .threadToStackTraceElement(threadToStackTraceElements)
                .build();

        dumpQueue.put(threadAndStackTrace);
    }

    /**
     * I.E. max is 1 seconds (1000 ms), but we dump every 100 ms, then max no. of dumps is 10.
     *
     * @return max number of items in the dump queue
     */
    private int calcMaxQueue() {
        var stopAllAfterMillis = agentConfig.getLimits().getStopAllWhenAggregateDumpsNotRunning().toMillis();
        return (int) (stopAllAfterMillis / agentConfig.getIntervals().getThreadDump().toMillis());
    }

    private boolean excludeIncludeThreads(String threadName) {
        var limits = agentConfig.getLimits();
        if (limits.getInclude() != null) {
            // Whitelist
            return limits.getInclude()
                    .stream()
                    .anyMatch(pattern -> pattern.matcher(threadName).matches());
        } else if (limits.getExclude() != null) {
            return limits.getExclude()
                    .stream()
                    .noneMatch(pattern -> pattern.matcher(threadName).matches());
        }

        return true;
    }
}
