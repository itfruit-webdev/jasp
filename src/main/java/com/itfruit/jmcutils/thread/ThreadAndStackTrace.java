package com.itfruit.jmcutils.thread;

import lombok.Builder;
import lombok.Data;

import java.util.List;
import java.util.Map;

@Data
@Builder
public class ThreadAndStackTrace {
    Long timeInNanos;
    Map<String, List<StackTraceElement>> threadToStackTraceElement;
}
