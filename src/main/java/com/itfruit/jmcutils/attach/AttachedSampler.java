package com.itfruit.jmcutils.attach;

import com.itfruit.jmcutils.aggregator.ResultsAggregateRotate;
import com.itfruit.jmcutils.aggregator.ThreadDumpAggregator;
import com.itfruit.jmcutils.config.AgentConfig;
import com.itfruit.jmcutils.thread.ThreadDumper;

import java.io.File;
import java.util.Optional;

public class AttachedSampler {
    private final ThreadDumper threadDumper;
    private final ThreadDumpAggregator aggregator;
    private final ResultsAggregateRotate resultsAggregateRotate;

    public AttachedSampler(AgentConfig agentConfig) {
        try {
            var outputSampleDir = new File(agentConfig.getResultPath());
            if (!outputSampleDir.exists() && !outputSampleDir.mkdirs()) {
                throw new Exception(String.format("Result path [%s] cannot be created", agentConfig.getResultPath()));
            }

            threadDumper = new ThreadDumper(agentConfig);
            aggregator = new ThreadDumpAggregator(agentConfig, threadDumper::retrieveDumps);
            resultsAggregateRotate = new ResultsAggregateRotate(agentConfig, aggregator::retrieveTrees);
        } catch (Exception e) {
            shutDownAll();
            throw new RuntimeException("Failed to attach and sample application", e);
        }
    }

    public void shutDownAll() {
        Optional.ofNullable(threadDumper).ifPresent(ThreadDumper::shutdown);
        Optional.ofNullable(aggregator).ifPresent(ThreadDumpAggregator::shutdown);
        Optional.ofNullable(resultsAggregateRotate).ifPresent(ResultsAggregateRotate::shutdown);
    }
}
