package com.itfruit.jmcutils.executors;

import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.ThreadPoolExecutor;

public final class ScheduledExecutorFactory {

    public static ScheduledExecutorService setupScheduledPool(String name) {
        var executor = new ScheduledThreadPoolExecutor(1, new NamedThreadFactory(name));
        executor.setRejectedExecutionHandler(new ThreadPoolExecutor.CallerRunsPolicy());
        return executor;
    }
}
