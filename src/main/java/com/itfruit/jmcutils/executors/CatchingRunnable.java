package com.itfruit.jmcutils.executors;

public class CatchingRunnable implements Runnable {

    private final RunnableWithException roRun;

    public CatchingRunnable(RunnableWithException toRun) {
        this.roRun = toRun;
    }

    @Override
    public void run() {
        try {
            roRun.run();
        } catch (InterruptedException ie) {
            Thread.currentThread().interrupt();
        } catch (Throwable t) {
            t.printStackTrace();
        }
    }

    @FunctionalInterface
    public interface RunnableWithException {
        void run() throws Exception;

        default RunnableWithException andThen(RunnableWithException after) {
            return () -> {
                run();
                after.run();
            };
        }
    }
}
