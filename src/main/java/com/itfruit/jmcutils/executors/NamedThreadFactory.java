package com.itfruit.jmcutils.executors;

import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;

public class NamedThreadFactory implements ThreadFactory {
    private static final String THREAD_NAMES_PREFIX = "Attached Agent Sampler";

    private final AtomicInteger threadNumber = new AtomicInteger(1);
    private final String name;

    public NamedThreadFactory(String name) {
        this.name = name;
    }

    @Override
    public Thread newThread(Runnable r) {
        String threadName = String.format("%s-%s:%s", THREAD_NAMES_PREFIX, name, threadNumber.getAndIncrement());
        var t = new Thread(r, threadName);
        t.setDaemon(true);
        return t;
    }

    public static boolean isAgentThread(String threadName) {
        return threadName.startsWith(THREAD_NAMES_PREFIX);
    }
}
