package com.itfruit.jmcutils;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.atomic.AtomicInteger;

public class TestClass {
    public static void main(String[] args) {
        ExecutorService executorService1 = Executors.newFixedThreadPool(500, r -> new Thread(r, "executor-hz"));
        ExecutorService executorService2 = Executors.newFixedThreadPool(5, r -> new Thread(r, "Cookbook-Processing"));
        ExecutorService executorService3 = Executors.newFixedThreadPool(5, r -> new Thread(r, "hz-async-1"));
        AtomicInteger no4 = new AtomicInteger(0);
        ExecutorService executorService4 = Executors.newFixedThreadPool(5, r -> new Thread(r, no4.getAndIncrement() + ":abcd"));
        ((ThreadPoolExecutor) Executors.newFixedThreadPool(5)).prestartAllCoreThreads();
        for (int i = 0; i < 500; i++) {
            executorService1.submit(TestClass::sleepTask);
        }
        for (int i = 0; i < 5; i++) {
            executorService2.submit(TestClass::sleepTask);
            executorService3.submit(TestClass::sleepTask);
            executorService4.submit(TestClass::sleepTask);
        }
    }

    private static Runnable sleepTask() {
        while (true) {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    //                    URL url = null;
//                    try {
//                        url = new URL("https://httpbin.org/get");
//                    } catch (MalformedURLException e) {
//                        e.printStackTrace();
//                    }
//                    HttpURLConnection con = null;
//                    try {
//                        con = (HttpURLConnection) url.openConnection();
//                        con.setRequestMethod("GET");
//                        int responseCode = con.getResponseCode();
//                        con.disconnect();
//                    } catch (IOException e) {
//                        e.printStackTrace();
//                    }


}
